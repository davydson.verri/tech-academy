import { Calculator } from "@qa-unit-test/calculator"

describe('Calculator', () =>{

    let sut: Calculator

    beforeAll(() =>{
        sut = new Calculator()
    })

    it('Should sum two values and return total', () => {

        // Arrange
        const value1 = 6
        const value2 = 2
        const expectedValue = value1 + value2
        
        // Assert
        expect(sut.sum(value1, value2)).toBe(expectedValue)

    })

    it('Should subtract two values and return diference', () => {

        // Arrange
        const value1 = 6
        const value2 = 2
        const expectedValue = value1 - value2

        // Assert
        expect(sut.subtract(value1, value2)).toBe(expectedValue)
    })

})

