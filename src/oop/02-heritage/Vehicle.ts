export abstract class Vehicle {

    private manufacturer: string
    private model: string
    private emptyWeight: number

    constructor(manufacturer: string, model: string, weight: number) {    
        this.manufacturer = manufacturer
        this.model = model
        this.emptyWeight = weight
    }
    
    public getManufacturer(): string{
        return this.manufacturer
    }

    public getModel(): string{
        return this.model
    }

    public getEmptyWeight(): string{
        return `${this.emptyWeight.toLocaleString('pt-BR')} kg`
    }
}

export abstract class Aircraft extends Vehicle {

    private maxPayload: number
    private maxAltitude: number
     
    constructor(manufacturer: string, model: string, emptyWeight: number, maxPayload: number, maxAltitude: number) {
        super(manufacturer, model, emptyWeight)
        this.maxPayload = maxPayload
        this.maxAltitude = maxAltitude
    }
}

export abstract class Airplane extends Aircraft {

    private maxFuel: number
    private flapPosition: number

    constructor(manufacturer: string, model: string, emptyWeight: number, maxPayload: number, maxAltitude: number, maxFuel: number) {
        super(manufacturer, model, emptyWeight, maxPayload, maxAltitude)
        this.maxFuel = maxFuel
        this.flapPosition = 0
    }

    public abstract turnOnEngines(): void
    public abstract turnOffEngines(): void
}

export abstract class Engine {
    
    protected engineNumber: number
    protected rpm: number
    protected fuelPressure: number
    
    public abstract TurnOn(): void
    public abstract TurnOff(): void
    
    constructor(engineNumber: number) {
        this.engineNumber = engineNumber
        this.rpm = 0
        this.fuelPressure = 0
    }

    public SetFuelPressure(fuelPressure: number): void {
        this.fuelPressure = fuelPressure
    }

    public GetFuelPressure(): number {
        return this.fuelPressure
    }

}

export class RollsRoyceTrent900 extends Engine {
        
    constructor(engineNumber: number) {
        super(engineNumber);        
    }

    public TurnOn(): void {
        if(this.fuelPressure < 10) {
            console.log(`Engine ${this.engineNumber} Low fuel pressure`)
            this.TurnOff()            
        }else{
            console.log(`Engine ${this.engineNumber} Started`)
        }
    }

    public TurnOff(): void {
        console.log(`Engine ${this.engineNumber} Turning Off`)        
        this.rpm = 0
    }

}

export class A380 extends Airplane {
    
    private engine1: RollsRoyceTrent900 
    private engine2: RollsRoyceTrent900 
    private engine3: RollsRoyceTrent900 
    private engine4: RollsRoyceTrent900 

    constructor() {
        super('Airbus', 'A380', 277000, 664000, 43000, 664000)
        this.engine1 = new RollsRoyceTrent900(1)
        this.engine2 = new RollsRoyceTrent900(2)
        this.engine3 = new RollsRoyceTrent900(3)
        this.engine4 = new RollsRoyceTrent900(4)
    }

    public turnOnFuelPump(): void {
        this.engine1.SetFuelPressure(12)
        this.engine2.SetFuelPressure(12)
        this.engine3.SetFuelPressure(12)
        this.engine4.SetFuelPressure(12)
        console.log('Fuel pump started')
    }

    public turnOffFuelPump(): void {
        this.engine1.SetFuelPressure(0)
        this.engine2.SetFuelPressure(0)
        this.engine3.SetFuelPressure(0)
        this.engine4.SetFuelPressure(0)
        console.log('Fuel pump turned Off')
    }

    public turnOnEngines(): void {
        console.log('Starting engines')
        this.engine1.TurnOn()
        this.engine2.TurnOn()
        this.engine3.TurnOn()
        this.engine4.TurnOn()
    }

    public turnOffEngines(): void {
        console.log('Turning Off engines')
        this.engine1.TurnOff()
        this.engine2.TurnOff()
        this.engine3.TurnOff()
        this.engine4.TurnOff()
    }
}

/*
const a380 = new A380()
a380.turnOnFuelPump()
a380.turnOnEngines()
a380.turnOffEngines()
a380.turnOffFuelPump()
*/