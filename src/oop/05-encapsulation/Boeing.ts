import { Airplane } from "../02-heritage/Vehicle";

export class B737 extends Airplane {

    // Este é "CAMPO"
    private _landingGear : boolean;

    // Esta é a "PROPRIEDADE" pública somente leitura
    public get landingGear() : boolean {
        return this._landingGear;
    }   

    constructor() {
        super('Boeing', '737-800', 41410, 23950, 41000, 26020);
        this._landingGear = true
    }
    
    // Este é um método
    public retractLandingGear(){
        console.log('Retracting landing gear')
        this._landingGear = false
        console.log('Landing gear retracted')
    }

    public extendLandingGear(){
        console.log('Extending landing gear')
        this._landingGear = true
        console.log('Landing gear Extended')
    }

    // Esta é uma função
    public getLandingGearState() : string {
        if(this.landingGear) return 'Landing gear state: extended'
        else return 'Landing gear state: retracted'
    }

    public turnOnEngines(): void {
        throw new Error("Method not implemented.");
    }
    public turnOffEngines(): void {
        throw new Error("Method not implemented.");
    }
}



