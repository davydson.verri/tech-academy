
export abstract class Animal {

    protected abstract makeSound(): void 
    protected abstract whoAmI(): void

}

export class Bee extends Animal {

    public makeSound(): void {
        console.log('ZZzzZZzzzZzzZzzZZzZz');
    }
    public whoAmI(): void {
        console.log("I'm a Bee");
    }
}

export class Bear extends Animal {

    public makeSound(): void {
        console.log('Groarrrrrrr');
    }
    public whoAmI(): void {
        console.log("I'm a Bear");
    }

}

