export class Animal{
    public Name: string;
    sleep():void{
        console.log('ZZZzzzZZZzzz')
    }
    wakeUp():void{
        console.log('AAAAhhhhhhhh')
    }
}

export class Bee extends Animal {
    public wingSize: number
    fly() : void{
        console.log('Weeeeee')
    }
}

export class Bear {
    public animal: Animal;
    public clawSize: number
    hibernate() : void{
        console.log('ZZZzzzzZZZzzzzZZZZzzzZZZzzzzZZZzzzzZZZzzzzZZZZzzzZZZzzzzZZZzzzzZZZzzzzZZZZzzzZZZzzzz')
    }
}

//Perguntar se a classe é uma superclasse

export class Case1{

    public Test(): void {

        var beeHeritage = new Bee() 
        beeHeritage.Name = 'Bee'
        beeHeritage.wingSize = 35
        beeHeritage.wakeUp()
        beeHeritage.fly()

        var bearComposition = new Bear()
        bearComposition.animal = new Animal()
        bearComposition.clawSize = 60
        bearComposition.animal.wakeUp()
        bearComposition.hibernate()
    }

}