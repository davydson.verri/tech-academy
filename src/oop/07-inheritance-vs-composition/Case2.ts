export class Person{
    public Name: string
}

export interface IRepository<T> {
    Add(entity: T): void
}

export interface IPersonRepository {
    Add(person: Person): void
}

export class Repository<T> implements IRepository<T>{
    Add(entity: T): void {
        console.log("Entity stored to database");
    }    
}

export class PersonHeritageRepository extends Repository<Person> implements IPersonRepository{

}

export class PersonCompositionRepository implements IPersonRepository{
    
    private readonly personRepo: IRepository<Person>
    
    constructor(_personRepo: IRepository<Person>) {        
        this.personRepo = _personRepo
    }

    Add(person: Person): void {
        this.personRepo.Add(person)
    }
}

export class HeritageVsComposition{

    public test() : void {

        var heritageRepo = new PersonHeritageRepository()
        heritageRepo.Add(new Person())

        var compositionRepo = new PersonCompositionRepository(new Repository<Person>())
        heritageRepo.Add(new Person())

    }

}
