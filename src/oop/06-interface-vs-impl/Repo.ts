
export interface IRepository {
    Add(): void
}

export class Repository implements IRepository {
    Add(): void {
        console.log("Added to database :D")
    }
}

export class MemoryRepository implements IRepository {    
    Add(): void {
        console.log("Added to memory :'(")
    }
}

export class Implementation{
    public Add(): void {
        const repo = new Repository()
        repo.Add();
    }
}

export class Abstraction {
    private readonly repo: IRepository

    constructor(_repo: IRepository) {        
        this.repo = _repo
    }

    public Add(): void{        
        this.repo.Add();
    }
}

export class RestAPI{
    private env: string = 'dev'
    public AddImplementation(): void{
        const repo = new Implementation()
        repo.Add()
    }

    public AddAbstraction(): void{
        if(this.env === 'dev'){
            const repo = new Abstraction(new Repository())
            repo.Add()
        }
        else{
            const memRepo = new Abstraction(new MemoryRepository())
            memRepo.Add()
        }
    }
}


