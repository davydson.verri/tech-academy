export class Car {
    
    // Propriedades
    private manufacturer: string
    private name: string
    private color: string
    private potency: number
    private seats: number
    
    // Construtor
    constructor(manufacturer: string, name: string,color: string, potency: number, seats: number) {
        this.manufacturer = manufacturer
        this.color = color
        this.potency = potency
        this.seats = seats    
    }
}