export class Calculator {

    public subtract(value1: number, value2: number): number {
        return value1 - value2
    }
    
    public sum(value1: number, value2: number): number {
        return value1 + value2
    }

}