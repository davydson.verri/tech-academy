import { A380 } from "./oop/02-heritage/Vehicle"
import { Bear, Bee } from "./oop/03-polymorphism/Animal"
import { B737 } from "./oop/05-encapsulation/Boeing"

/*
const car = new Car('Porshe', '718 Cayman GT4', 'Yellow', 420, 2)
*/

/*
const a380 = new A380()
console.log(`Airplane Manufacturer: ${a380.getManufacturer()}`)
console.log(`Airplane Model: ${a380.getModel()}`)
console.log(`Airplane EmptyWeight: ${a380.getEmptyWeight()}`)
*/

/*
const bear = new Bear()
bear.whoAmI()
bear.makeSound()
console.log('')
console.log('-------')
console.log('')
const bee = new Bee()
bee.whoAmI()
bee.makeSound()
*/

const b737 = new B737()
console.log(b737.getLandingGearState())
b737.retractLandingGear()
console.log(b737.getLandingGearState())
b737.extendLandingGear()
console.log(b737.getLandingGearState())

